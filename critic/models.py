from django.db import models

class Game(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=200)
    score = models.IntegerField(default=0)

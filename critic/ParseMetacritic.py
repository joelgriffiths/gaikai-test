#!/usr/bin/env python3

import urllib.request
from bs4 import BeautifulSoup
import json

class ParseMetacritic(object):

    url = None
    results = None

    def __init__(
            self,
            remote_url='http://www.metacritic.com/game/playstation-3'
        ):
        """ Specify which remote_uri to use """

        self.url = remote_url

    def getResults(self):
        """
        Returns a list of tuples

        (score, name)
        """

        # Don't get the results if it's already been done
        if self.results != None:
            return(self.results)

        request = urllib.request.Request(
            self.url,
            headers={'User-Agent': 'Mozilla/5.0'}
        )

        with urllib.request.urlopen(request) as response:
            content = response.read()

        # Use BeautifulSoup to parse website
        soup = BeautifulSoup(content, "html.parser")
        titles = soup.find_all("h3", { "class": 'product_title'})
        scores = soup.find_all("span", { "class": 'metascore_w', "class": 'medium'})

        # Combine titles and scores into a list
        # zip was giving me fits
        results = list()
        for idx, title in enumerate(titles):
            results.append({
                "score": int(scores[idx].string),
                "title": str(titles[idx].a.string)
            })

        self.results = results
        return(results)

    def getGameScore(self, game_name):
        """ Get the score of the game """

        results = self.getResults()

        for game in results:
            if game_name == game['title']:
                score = game['score']
                return score

        return None

    def getJSONResults(self, max_results = 10):
        """
        Get the parsed metacritic results in json
        
        Returns the number of results UP TO the max_results,
        but it can return less.
        """

        # Don't worry, this gets the results once only
        results = self.getResults()

        json_out = json.dumps(list(results[0:max_results]))
        return json_out

    def getJSONResultsForGame(self, game_name):
        """
        Returns the JSON results for a specific game.

        Returns an empty JSON object if there is no match.
        """

        score = self.getGameScore(game_name)

        if score == None:
            json_out = json.dumps({})
        else:
            json_out = json.dumps(
                {'title': game_name, 'score': score}
            )

        return json_out

        
# Basic functional tests for debugging
if __name__ == '__main__':
    pmr = ParseMetacritic()
    results = pmr.getResults()
    json_results = pmr.getJSONResults(3)

    print(json_results)

    for game in results:
        print("Score %d: %s" % (game['score'], game['title']))

    game_json_results = pmr.getJSONResultsForGame(
                            "Yakuza 5"
                        )
    print(game_json_results)


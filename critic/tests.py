

from django.test import TestCase
from django.http import HttpRequest
from critic.views import get_game_info, get_top_three
from urllib.request import pathname2url
import json

class ParseMetacriticTest(TestCase):

    def get_top_games(self):
        """ Tests get top games """

        print("Retrieving Top Games")
        request = HttpRequest()
        request.path = "/games"
        response = get_top_three(request)

        game_data = json.loads(response.content.decode("utf-8"))

        print(game_data)

        title = game_data[2]['title']
        score = game_data[2]['score']

        self.assertTrue(title)
        self.assertTrue(score)

        return(title, score)

    def test_get_top_info(self):
        """ Tests that game info is returned """

        # Start by getting the last game of 3
        title, score = self.get_top_games()

        # Now get the game info
        request = HttpRequest()
        request.path = pathname2url("/games/" + title)
        print("Getting %s" % request.path)
        response = get_game_info(request)

        game_info = json.loads(response.content.decode("utf-8"))
        print(game_info)
        self.assertEqual(score, game_info['score'])
        self.assertEqual(title, game_info['title'])

    def test_split(self):
        s = 'hello world'
        self.assertEqual(s.split(), ['hello', 'world'])
        # check that s.split fails when the separator is not a string
        with self.assertRaises(TypeError):
            s.split(2)

if __name__ == '__main__':
    unittest.main()




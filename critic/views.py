#from django.shortcuts import render
from critic.models import Game
from critic.ParseMetacritic import ParseMetacritic

#from html.parser import HTMLParser
from urllib.request import urlopen, unquote

#from django.shortcuts import render
from django.http import HttpResponse

def get_top_three(request):

    # TODO - Store results in a DB
    pmc = ParseMetacritic()

    # Get the top 3
    # TODO - Add URL Parameters for count
    json_results = pmc.getJSONResults(3)

    return HttpResponse(json_results, content_type="application/json")

def get_game_info(request):

    # Get game, remove URL escapes
    game = unquote(request.path).split('/')[-1]

    # TODO - Store results in a DB
    pmc = ParseMetacritic()
    score = pmc.getGameScore(game)

    json_results = pmc.getJSONResultsForGame(game)

    return HttpResponse(json_results, content_type="application/json")


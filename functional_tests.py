#!/usr/bin/env python3

from selenium import webdriver
import unittest
import json
import time
from urllib.request import urlopen, unquote, pathname2url

class MetacriticParser(unittest.TestCase):
    """ MetacriticParser Performs Function Testing for Game Lists """

    def setUp(self):
        self.ff = webdriver.Firefox()
        self.ff.implicitly_wait(3)

    def tearDown(self):
        self.ff.quit()

    def get_games(self):
        """ Test the /game URL returns top3 metacritic games """

        print("Getting JSON list of games")
        content = self.ff.get('http://localhost:8000/games')
        time.sleep(2)
        content = self.ff.find_element_by_tag_name("pre").text
        game_data = json.loads(content)

        self.assertTrue(game_data)

        game_to_test  = game_data[1]['title']
        score_to_test = game_data[1]['score']

        print("Second Game = %s, Score: %d" % (
            game_to_test,
            score_to_test))

        # Use True to test if not empty
        self.assertTrue(game_to_test)

        # Use True to determine in not 0
        self.assertTrue(bool(score_to_test))

        return(game_to_test, score_to_test)

    def test_get_specific_game(self):
        """
        Test /games/TITLE_OF_GAME_GOES_HERE

        Use second response from test_get_games

        This runs get_games as well
        """

        print("Calling get_games")
        game_to_test, score_to_test = self.get_games()

        print("Using %s to test game info page" % game_to_test)
        quoted_game = pathname2url(game_to_test)
        url = "http://localhost:8000/games/" + quoted_game

        print("Fetching %s", url)
        self.ff.get(url)
        time.sleep(2)
        content = self.ff.find_element_by_tag_name("pre")
        game_info = json.loads(content.text)

        self.assertEqual(game_info["title"], game_to_test)

        self.assertEqual(game_info["score"], score_to_test)
        

if __name__ == '__main__':
    unittest.main()



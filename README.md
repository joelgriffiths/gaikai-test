# gaikai

This API provides the Metacritic HTML parsing algorithm
to retrieve a list of top games (PS3) and display them
iun JSON format using an API.

This django wrapped API provides the following URLS:

* Get Metacritic top 3 games and scores in JSON
http://host.exmaple.com/games

Results:
[{'title': GAME_NAME, 'score': GAME_SCORE}, ...]

* Get Metacritic score of a game in JSON
http://host.example.com/games/GAME_NAME

Results:
{'title': GAME_NAME, 'score': GAME_SCORE}

## To Test:

(From project directory ie ~/gaikai

```sh
$ source bin/activate
(gaikai)$ python manage.py runserver
```

(From a local browser)
http://127.0.0.1:8000/games

http://127.0.0.1:8000/games/Steins;Gate

## Functional Testing

Functional testing completes the following:

* Retrieves a list of Games via the API.
* Uses the second game returned  and retrieves the game information

```sh
$ # If server is running
$ python manage.py test functional_tests
$
$ # Run at any time
$ python manage.py test critic
```

## Additional user Modules

pydoc critic.ParseMetacritic

Help on module critic.ParseMetacritic in critic:

NAME
    critic.ParseMetacritic

CLASSES
    builtins.object
        ParseMetacritic

    class ParseMetacritic(builtins.object)
     |  Methods defined here:
     |
     |  __init__(self, remote_url='http://www.metacritic.com/game/playstation-3')
     |      Specify which remote_uri to use
     |
     |  getGameScore(self, game_name)
     |      Get the score of the game
     |
     |  getJSONResults(self, max_results=10)
     |      Get the parsed metacritic results in json
     |
     |      Returns the number of results UP TO the max_results,
     |      but it can return less.
     |
     |  getJSONResultsForGame(self, game_name)
     |      Returns the JSON results for a specific game.
     |
     |      Returns an empty JSON object if there is no match.
     |
     |  getResults(self)
     |      Returns a list of tuples
     |
     |      (score, name)
     |
     |  ----------------------------------------------------------------------
     |  Data descriptors defined here:
     |
     |  __dict__
     |      dictionary for instance variables (if defined)
     |
     |  __weakref__
     |      list of weak references to the object (if defined)
     |
     |  ----------------------------------------------------------------------
     |  Data and other attributes defined here:
     |
     |  results = None
     |
     |  url = None

FILE
    /home/jgriffiths/gaikai/critic/ParseMetacritic.py


## Primary Imports:

* [selenium](https://pypi.python.org/pypi/selenium)
* [BeautifulSoap](http://www.crummy.com/software/BeautifulSoup/)

## Requirements

* [python3](https://www.python.org/downloads/release/python-343/)
